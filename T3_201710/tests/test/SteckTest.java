package test;

import model.data_structures.Stack;
import junit.framework.TestCase;

public class SteckTest extends TestCase
{
	private Stack<String> stack1;
	
	public void setupEscenario1()
	{
		try
		{
			stack1 = new Stack();
		}
		catch(Exception E)
		{
			fail("No deberia lanzar Excepci�n");
		}
	}
	
	public void testPush()
	{
		setupEscenario1();
		stack1.push("Element A");
		stack1.push("Element B");
		stack1.push("Element C");
		assertEquals("Element C", stack1.darElemento(0));
		assertEquals("Element B", stack1.darElemento(1));
		assertEquals("Element A", stack1.darElemento(2));
	}
	
	public void testPop()
	{
		setupEscenario1();
		stack1.push("Element A");
		stack1.push("Element B");
		stack1.push("Element C");
		stack1.pop();
		assertEquals("Element B", stack1.darElemento(0));
		stack1.pop();
		assertEquals("Element A", stack1.darElemento(0));
	}
}
