package model.logic;

import java.util.ArrayList;

import model.data_structures.Queue;
import model.data_structures.Stack;

public class Manejador implements Comparable<Character>
{
	public static final char ABRE_PARENTESIS = '(';
	public static final char CIERRA_PARENTESIS = ')'; 
	public static final char ABRE_CAJA = '['; 
	public static final char CIERRA_CAJA = ']'; 
	public static final char PLUS = '+'; 
	public static final char MINUS = '-'; 
	public static final char TIMES = '*'; 
	public static final char OVER = '/'; 
	private String a; 
	Stack<Character> stackX = new Stack<>(); 


	public boolean numeroIgual(String formula)
	{
		boolean aRetornar=false; 
		int auxAbreParentesis = 0; 
		int auxCierraParentesis = 0; 
		int auxAbreCaja = 0; 
		int auxCierraCaja = 0; 
		int total = 0;
		char[] arrayDeFormula = formula.toCharArray(); 
		for (int i = 0; i<arrayDeFormula.length;i++)
		{
			char actual = arrayDeFormula[i]; 
			if (actual == ABRE_PARENTESIS){auxAbreParentesis++;}
			if (actual == CIERRA_PARENTESIS){auxCierraParentesis++;}
			if (actual == ABRE_CAJA){auxAbreCaja++;}
			if (actual == CIERRA_CAJA){auxCierraCaja++;}
		}
		if (auxAbreParentesis==auxCierraParentesis)
		{
			if (auxAbreCaja==auxCierraCaja)
			{
				aRetornar= true;
			}
		}
		return aRetornar; 

	}
	public boolean parentesisSeguidos(String expresion)
	{
		char[] expresionArray = expresion.toCharArray();
		int count=0;
		for(int i=0; i<expresionArray.length;i++)
		{
			char actual = expresionArray[i];
			char siguiente = expresionArray[i+1];
			if(actual==ABRE_PARENTESIS&&siguiente==CIERRA_PARENTESIS)
			{
				count++;
			}
			if(actual==ABRE_CAJA &&actual==CIERRA_CAJA)
			{
				count++;
			}
		}
		if(count==0)
		{
			return true;
		}
		else
			return false;
	}
	public boolean dosSimbolosSeguidos(String expresion)
	{
		char[] expresionArray = expresion.toCharArray();
		int count=0;
		for(int i = 0; i<expresionArray.length;i++)
		{
			char actual = expresionArray[i];
			char siguiente = expresionArray[i+1];
			if(esSimbolo(actual)&&esSimbolo(siguiente))
			{
				count++;
			}
		}
		if(count==0)
		{
			return true; 
		}
		else
			return false;
	}
	public boolean soloSimbolos(String expresion)
	{
		char[] expresionArray = expresion.toCharArray();
		int count = 0;
		for(int i = 0; i<expresionArray.length;i++)
		{
			char actual = expresionArray[i];
			if(esNumero(actual))
			{
				count++;
			}
		}
		if(count==0)
		{
			return true;
		}
		else
			return false;
	}
	public boolean malCerrado(String expresion)
	{
		ArrayList posicionesAbiertosCuadrados = new ArrayList();
		ArrayList posicionesAbiertosRedondos = new ArrayList();
		ArrayList posicionesCerradosCuadrados = new ArrayList();
		ArrayList posicionesCerradosRedondos = new ArrayList();
		int count = 0;
		char[] expresionArray = expresion.toCharArray();
		for(int i=0;i<expresionArray.length;i++)
		{
			char actual = expresionArray[i];
			if(actual==ABRE_CAJA)
			{
				posicionesAbiertosCuadrados.add(i);
			}
			if(actual==CIERRA_CAJA)
			{
				posicionesCerradosCuadrados.add(i);
			}
			if(actual==ABRE_PARENTESIS)
			{
				posicionesAbiertosRedondos.add(i);
			}
			if(actual==CIERRA_PARENTESIS)
			{
				posicionesAbiertosRedondos.add(i);
			}
		}
		if((Integer) posicionesAbiertosCuadrados.get(0)>(Integer) posicionesAbiertosRedondos.get(0))
		{
			count++;
		}

		if((Integer) posicionesCerradosCuadrados.get(posicionesCerradosCuadrados.size())<(Integer) posicionesCerradosRedondos.get(posicionesCerradosRedondos.size()))
		{
			count++;
		}

		if(count==0)
			return true;
		else
			return false;

	}
	public boolean esSimbolo(char simbolo)
	{
		if(simbolo==PLUS)
			return true;
		if(simbolo==MINUS)
			return true;
		if(simbolo==TIMES)
			return true;
		if(simbolo==OVER)
			return true;
		else
			return false;
	}
	public boolean esParentesis(char simbolo)
	{
		if(simbolo==ABRE_PARENTESIS)
			return true;
		if(simbolo==CIERRA_PARENTESIS)
			return true;
		if(simbolo==ABRE_CAJA)
			return true;
		if(simbolo==CIERRA_CAJA)
			return true;
		else 
			return false;
	}
	public boolean esNumero(char simbolo)
	{
		if(simbolo=='0')
			return true;
		if(simbolo=='1')
			return true;
		if(simbolo=='2')
			return true;
		if(simbolo=='3')
			return true;
		if(simbolo=='4')
			return true;
		if(simbolo=='5')
			return true;
		if(simbolo=='6')
			return true;
		if(simbolo=='7')
			return true;
		if(simbolo=='8')
			return true;
		if(simbolo=='9')
			return true;
		else
			return false;
	}
	public <T> Queue ordenarPila(Stack<Character> pila)
	{
		
		Stack retorno = new Stack();
		Queue<T> aux = new Queue<>();
		for(int i=0;i<pila.size();i++)
		{
			int minIndex = 0;
			for(int j=i+1;j<pila.size();j++)
			{
				if(pila.darElemento(j).compareTo(pila.darElemento(minIndex))<0)
				{
					minIndex=j;
				}
			}
			aux.push((T) pila.darElemento(minIndex));
		}
		return aux;
	}
	public int compareTo(Character aComparar) 
	{
		int valorcomparacion = 0;
		char[] auxiliar = this.toString().toCharArray();
		char auxiliar2 = auxiliar[0];
		//Comparaciones cuadrado cerrado
		if(auxiliar2==CIERRA_CAJA)
		{
			if(aComparar==CIERRA_CAJA)
			{
				valorcomparacion=0;
			}
			if(aComparar!=CIERRA_CAJA)
			{
				valorcomparacion=-1;
			}
		}
		else if(auxiliar2==CIERRA_PARENTESIS)
		{
			if(aComparar==CIERRA_CAJA)
			{
				valorcomparacion=1;
			}
			if(aComparar==CIERRA_PARENTESIS)
			{
				valorcomparacion=0;
			}
			else
				valorcomparacion=-1;
		}
		else if(auxiliar2==ABRE_CAJA)
		{
			if(aComparar==CIERRA_CAJA)
			{
				valorcomparacion=1;
			}
			if(aComparar==CIERRA_PARENTESIS)
			{
				valorcomparacion=1;
			}
			if(aComparar==ABRE_CAJA)
			{
				valorcomparacion=0;
			}
			else
				valorcomparacion=-1;
		}
		else if(auxiliar2==ABRE_PARENTESIS)
		{
			if(aComparar==CIERRA_CAJA)
			{
				valorcomparacion=1;
			}
			if(aComparar==CIERRA_PARENTESIS)
			{
				valorcomparacion=1;
			}
			if(aComparar==ABRE_CAJA)
			{
				valorcomparacion=1;
			}
			if(aComparar==ABRE_PARENTESIS)
			{
				valorcomparacion=0;
			}
			else
				valorcomparacion=-1;
		}
		else if(esSimbolo(auxiliar2)==true)
		{
			if(esParentesis(aComparar)==true)
			{
				valorcomparacion=1;
			}
			if(esSimbolo(aComparar)==true)
			{
				valorcomparacion=0;
			}
			if(esNumero(aComparar)==true)
			{
				valorcomparacion=-1;
			}
		}
		else if(esNumero(auxiliar2)==true)
		{
			if(esParentesis(aComparar)==true)
			{
				valorcomparacion=1;
			}
			if(esSimbolo(aComparar)==true)
			{
				valorcomparacion=1;
			}
			if(esNumero(aComparar)==true)
			{
				valorcomparacion=0;
			}
		}
		return valorcomparacion;
	}
	

	public boolean expresionBienFormada(String expresion)
	{
		char[] expresionArray = expresion.toCharArray();
		boolean expr = false;
		
		if(numeroIgual(expresion)==true);
		{
			if(parentesisSeguidos(expresion)==true)
			{
				if(dosSimbolosSeguidos(expresion)==true)
				{
					if(soloSimbolos(expresion))
					{
						if(malCerrado(expresion))
						{
							expr=true;
						}
					}
				}
			}
		}
		
		if(expr==true)
		{
			for(int i=0;i<expresionArray.length;i++)
			{
				stackX.push(expresionArray[i]);
			}
		}
		
		return expr;
	}
	


}
