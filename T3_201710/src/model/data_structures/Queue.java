package model.data_structures;

import api.IQueue;

public class Queue<T> implements IQueue<T> 
{
	ListaEncadenada<T> linked; 
	@Override
	public void stack() {linked=new ListaEncadenada<>();}

	@Override
	public void push(T item) {linked.agregarElementoFinal(item);}

	@Override
	public T pop() {return (T) linked.eliminarPrimero();}

	@Override
	public boolean isEmpty() 
	{
		if (linked.darNumeroElementos()==0)
		{
			return true;
		}
		else 
			return false; 
	}

	@Override
	public int size() {return linked.darNumeroElementos();}

}
