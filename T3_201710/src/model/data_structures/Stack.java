package model.data_structures;

import api.IStack;

public class Stack<T> implements IStack<T>
{
	ListaEncadenada<T> linked; 
	@Override
	public void stack() {linked=new ListaEncadenada<>();}

	@Override
	public void push(T item) {linked.agregarElementoPrincipio(item);}

	@Override
	public T pop() { return (T) linked.eliminarPrimero();}

	@Override
	public boolean isEmpty() 
	{
		if (linked.darNumeroElementos() == 0)
		{
			return true;
		}
		else 
			return false; 
	}

	@Override
	public int size() {return linked.size();}
	public T darElemento(int a){return (T) linked.darElemento(a);}

}
