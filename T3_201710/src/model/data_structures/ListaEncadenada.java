package model.data_structures;

import java.util.Iterator;


public class ListaEncadenada<T> implements ILista<T> {

	private NodoSimple<T> primero;
	private NodoSimple<T> actual;
	private int listSize;

	public ListaEncadenada()
	{
		primero = new NodoSimple<T>();
		actual = primero;
	}

	public int size()
	{
		return listSize;
	}

	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>() 
				{
			private NodoSimple<T> actualT=null;

			public boolean hasNext() 
			{
				if(actualT==null)return primero.getItem()!=null;
				else return actualT.getNext() != null;
			}

			public T next() 
			{
				if(actualT==null)
				{
					actualT=primero;
					if(actualT==null)return null;
					else return actualT.getItem();
				}
				else
				{
					actualT = actualT.getNext();
					return actualT.getItem();
				}

			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
				};
	}

	@Override
	public void agregarElementoFinal(T elem) 
	{
		NodoSimple<T> newNode = new NodoSimple<>(); 
		if (primero == null){
			primero=newNode;
		}
		else{
			NodoSimple<T>actual = primero; 
			while(actual.getNext()!=null){
				actual=actual.getNext();
			}
			actual.setNext(newNode);
		}
		listSize++; 
	}
	public void agregarElementoPrincipio(T elem)
	{
		NodoSimple<T> newNode = new NodoSimple<>();
		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			return;
		}
		else
		{
			newNode.setNext(primero);
			primero=newNode;
		}
		listSize++;
	}

	public boolean estaVacia()
	{
		return primero == null; 
	}
	public T eliminarPrimero()
	{
		if(primero == null)
		{
			System.out.println("No hay elementos");
		}
		T elem = primero.getItem();
		NodoSimple<T> siguientePrimero = primero.getNext();
		primero.setItem(null);
		primero = siguientePrimero;
		listSize--;
		return elem;

	}
	public void eliminarUltimo()
	{
		NodoSimple<T>aux = primero; 
		if (aux.getNext()==null)
		{
			primero=null; 
		}
		if (!estaVacia())
		{
			aux=primero; 
		}
		while(aux.getNext().getNext() != null){
			aux=aux.getNext(); 
		}
		aux.setNodoSimple(null);
	}

	@Override
	public T darElemento(int pos) 
	{
		actual = primero;
		int contador = 0;
		while(contador<pos)
		{
			actual = actual.getNext();
			contador++;
		}
		return (T) actual.getItem();
	}

	@Override
	public int darNumeroElementos() 
	{
		int contador = 0;
		NodoSimple<T> act = primero;
		while(act != null)
		{
			contador ++;
			act = act.getNext();
		}
		return contador;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		if(actual!=null)
		{
			return actual.getItem();
		}
		else
			return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		NodoSimple<T> act = primero;
		while(act != null)
		{
			if (act.getNext() != null && act.getNext().equals(actual))
			{
				actual = act;
				return true;
			}
			act = act.getNext();
		}
		return false;
	}

}
